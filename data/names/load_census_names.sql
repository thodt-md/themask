\set AUTOCOMMIT OFF
\cd :workdir

CREATE TEMPORARY TABLE census_name (
  name            VARCHAR(25),
  freq            REAL,
  cumulative_freq REAL,
  rank            INT
);

\copy census_name FROM 'dist.female.first.csv' WITH (FORMAT CSV, HEADER, DELIMITER ',');

INSERT INTO themask.popular_name (name_type, rank, name)
  SELECT
    'F',
    cn.rank,
    cn.name
  FROM census_name cn;

TRUNCATE census_name;

\copy census_name FROM 'dist.all.last.csv' WITH (FORMAT CSV, HEADER, DELIMITER ',');

INSERT INTO themask.popular_name (name_type, rank, name)
  SELECT
    'L',
    cn.rank,
    cn.name
  FROM census_name cn;

TRUNCATE census_name;

\copy census_name FROM 'dist.male.first.csv' WITH (FORMAT CSV, HEADER, DELIMITER ',');

INSERT INTO themask.popular_name (name_type, rank, name)
  SELECT
    'M',
    cn.rank,
    cn.name
  FROM census_name cn;

TRUNCATE census_name;

DROP TABLE census_name;

COMMIT;

\set AUTOCOMMIT ON
