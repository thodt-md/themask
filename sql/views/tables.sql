CREATE VIEW themask._tables AS
  SELECT
    c.oid     AS table_oid,
    n.nspname AS schema_name,
    c.relname AS table_name
  FROM pg_class c
    LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE c.relkind = 'r' :: CHAR;
