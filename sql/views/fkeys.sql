CREATE VIEW themask._fkeys AS
  WITH all_fkeys AS (
      SELECT
        confrelid         AS pkey_table_oid,
        conrelid          AS fkey_table_oid,
        pkey_attr.attname AS pkey_column,
        fkey_attr.attname AS fkey_column
      FROM
        pg_attribute pkey_attr, pg_attribute fkey_attr,
        (SELECT
           conrelid,
           confrelid,
           conkey,
           confkey
         FROM themask._con_unfolded
         WHERE contype = 'f') constraints
      WHERE pkey_attr.attnum = confkey AND pkey_attr.attrelid = confrelid
            AND fkey_attr.attnum = conkey AND fkey_attr.attrelid = conrelid
  )
  SELECT
    tbl1.schema_name AS pkey_schema,
    tbl1.table_name  AS pkey_table,
    afk.pkey_column,
    tbl2.schema_name AS fkey_schema,
    tbl2.table_name  AS fkey_table,
    afk.fkey_column
  FROM
    all_fkeys afk, themask._tables tbl1, themask._tables tbl2
  WHERE
    afk.pkey_table_oid = tbl1.table_oid AND afk.fkey_table_oid = tbl2.table_oid;
