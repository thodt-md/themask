CREATE VIEW themask._col_defs AS
  SELECT
    c.table_schema :: TEXT    AS schema_name,
    c.table_name :: TEXT      AS table_name,
    c.ordinal_position :: INT AS column_position,
    c.column_name :: TEXT     AS column_name,
    c.udt_name :: TEXT        AS column_simple_type,
    CASE
    WHEN c.character_maximum_length IS NOT NULL
      THEN c.udt_name || '(' || c.character_maximum_length || ')'
    WHEN c.character_maximum_length IS NULL AND c.udt_name = 'numeric' AND c.numeric_precision IS NOT NULL
         AND c.numeric_scale IS NOT NULL
      THEN c.udt_name || '(' || c.numeric_precision || ',' || c.numeric_scale || ')'
    ELSE c.udt_name
    END :: TEXT               AS column_type
  FROM information_schema.columns c
  WHERE c.table_schema NOT IN ('pg_catalog', 'information_schema')
  ORDER BY schema_name, table_name, column_position;
