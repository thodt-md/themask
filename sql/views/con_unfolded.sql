CREATE VIEW themask._con_unfolded AS
  SELECT
    conrelid,
    confrelid,
    conkey [i]  AS conkey,
    confkey [i] AS confkey,
    contype
  FROM (
         SELECT
           conrelid,
           confrelid,
           conkey,
           confkey,
           contype,
           i
         FROM (
                SELECT
                  conrelid,
                  confrelid,
                  conkey,
                  confkey,
                  contype,
                  generate_series(1, array_upper(conkey, 1)) AS i
                FROM pg_constraint
              ) indexed
       ) unfolded;
