CREATE TABLE themask.compiled_operation (
  context_name    VARCHAR(20) NOT NULL REFERENCES themask.mask_context (name),
  operation_order INT         NOT NULL,
  operation_sql   TEXT        NOT NULL,
  PRIMARY KEY (context_name, operation_order)
);
