CREATE TABLE themask.table_policy (
  context_name                    VARCHAR(20)              NOT NULL REFERENCES themask.mask_context (name),
  schema_name                     VARCHAR(63)              NOT NULL,
  table_name                      VARCHAR(63)              NOT NULL,
  default_column_copy_mode        themask.MASK_DEFAULT_COLUMN_MODE NOT NULL,
  default_numeric_noise_fraction  DOUBLE PRECISION         NOT NULL,
  default_date_noise_days         INT                      NOT NULL,
  default_timestamp_noise_seconds DOUBLE PRECISION         NOT NULL,
  PRIMARY KEY (context_name, schema_name, table_name),
  CONSTRAINT positive_numeric_noise_fraction CHECK (default_numeric_noise_fraction > 0),
  CONSTRAINT positive_date_noise_days CHECK (default_date_noise_days > 0),
  CONSTRAINT positive_timestamp_noise_seconds CHECK (default_timestamp_noise_seconds > 0)
);
