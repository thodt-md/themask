CREATE TYPE themask.MASK_OPERATION AS ENUM ('copy', 'nullify', 'literal', 'noise', 'random', 'truncate', 'mask', 'lut', 'random_lut', 'shuffle');
