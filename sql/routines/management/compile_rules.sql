CREATE FUNCTION themask.compile_rules(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _sql_create_schemas_call CONSTANT TEXT := 'SELECT themask._create_masked_schemas(%L)';
  _sql_create_tables_call CONSTANT  TEXT := 'SELECT themask._create_masked_tables(%L)';
  _sql_restore_constraints_call CONSTANT  TEXT := 'SELECT themask._restore_constraints(%L)';

  _context                          themask.MASK_CONTEXT;
  _table_policy                     themask.TABLE_POLICY;
  _counter                          INT;
BEGIN
  _context := themask._get_context(_context_name);

  RAISE NOTICE 'compiling rules for masking context %s', _context_name;

  PERFORM themask._check_mandatory_column_counts(_context_name);
  PERFORM themask._check_pkey_methods(_context.name, _context.fail_on_pkey_fkey_checks);
  PERFORM themask._check_shuffle_has_pkeys(_context_name);
  PERFORM themask._check_fkey_methods(_context.name, _context.fail_on_pkey_fkey_checks);

  DELETE FROM themask.compiled_operation
  WHERE context_name = _context_name;

  PERFORM themask._add_compiled_operation(_context_name, format(_sql_create_schemas_call, _context_name));
  PERFORM themask._add_compiled_operation(_context_name, format(_sql_create_tables_call, _context_name));

  PERFORM themask._add_independent_shuffle_columns(_context_name);
  PERFORM themask._add_independent_random_lut_columns(_context_name);

  FOR _table_policy IN SELECT *
                       FROM themask.table_policy
                       WHERE context_name = _context_name
                       ORDER BY schema_name, table_name
  LOOP
    RAISE INFO 'compiling rules for %.%', quote_ident(_table_policy.schema_name), quote_ident(_table_policy.table_name);
    PERFORM themask._compile_rules(_table_policy);
  END LOOP;

  PERFORM themask._add_compiled_operation(_context_name, format(_sql_restore_constraints_call, _context_name));

  SELECT count(*)
  INTO _counter
  FROM themask.compiled_operation
  WHERE context_name = _context_name;
  RAISE NOTICE '% masking rule(s) added for masking context %', _counter, _context_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;


CREATE FUNCTION themask._compile_rules(_policy themask.TABLE_POLICY, _table_alias TEXT DEFAULT 'a')
  RETURNS VOID AS
$$
DECLARE
  _insert_sql CONSTANT        TEXT := 'INSERT INTO %1$I.%2$I (%3$s) SELECT %4$s FROM %5$I.%2$I %6$s';

  _context_name               TEXT NOT NULL := _policy.context_name;
  _schema_name                TEXT NOT NULL := _policy.schema_name;
  _table_name                 TEXT NOT NULL := _policy.table_name;

  _source_table_cols_fragment TEXT;

  _population_fragment        TEXT;
  _population_fragments       TEXT [];

  _select_fragment            TEXT;
  _masked_schema_name         TEXT;
  _final_sql                  TEXT;

  _operation                  themask._DEFAULT_OPERATIONS;
BEGIN
  IF themask._check_table_nop(_context_name, _schema_name, _table_name)
  THEN
    RAISE WARNING 'all columns for %.% are set as nullify, will insert nothing',
    quote_ident(_schema_name), quote_ident(_table_name);
    RETURN;
  END IF;

  SELECT string_agg(quote_ident(column_name), ', ')
  INTO STRICT _source_table_cols_fragment
  FROM themask._default_operations
  WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name;

  _masked_schema_name := themask._get_masked_schema_name(_context_name, _schema_name);
  ASSERT _masked_schema_name IS NOT NULL, 'masked schema name cannot be NULL';

  FOR _operation IN SELECT *
                    FROM themask._default_operations
                    WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name
                    ORDER BY column_position
  LOOP
    _population_fragment := themask._get_population_fragment(_operation, _table_alias);

    IF _population_fragment IS NULL
    THEN
      RAISE EXCEPTION 'unable to make masking function for %.%.%', quote_ident(_schema_name),
      quote_ident(_table_name), quote_ident(_operation.column_name)
      USING HINT = 'wrong type or not enough valid arguments';
    END IF;

    _population_fragments := _population_fragments || format('%s AS %I', _population_fragment, _operation.column_name);
  END LOOP;

  _select_fragment := array_to_string(_population_fragments, ', ');

  _final_sql := format(_insert_sql, _masked_schema_name, _table_name, _source_table_cols_fragment,
                       _select_fragment, _schema_name, _table_alias);

  PERFORM themask._add_compiled_operation(_context_name, _final_sql);
END;
$$
VOLATILE
LANGUAGE plpgsql;
