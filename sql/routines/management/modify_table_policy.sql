CREATE FUNCTION themask.modify_table_policy(_context_name           TEXT, _schema_name TEXT, _table_name TEXT,
                                            column_copy_mode        themask.MASK_DEFAULT_COLUMN_MODE DEFAULT NULL,
                                            numeric_noise_fraction  DOUBLE PRECISION DEFAULT NULL,
                                            date_noise_days         INT DEFAULT NULL,
                                            timestamp_noise_seconds DOUBLE PRECISION DEFAULT NULL
)
  RETURNS VOID AS
$$
DECLARE
  _sql CONSTANT TEXT := 'UPDATE themask.table_policy SET %s WHERE context_name = %L AND schema_name = %L' ||
                        ' AND table_name = %L';
  _options      TEXT [];
BEGIN
  ASSERT themask._check_table_policy_exists(_context_name, _schema_name, _table_name), 'policy for the table not found';
  ASSERT themask._obj_exists(_schema_name, _table_name), 'specified table does not exist';

  RAISE NOTICE 'modifying %.% table policy for masking context %', quote_ident(_schema_name), quote_ident(_table_name),
  _context_name;

  IF column_copy_mode IS NOT NULL
  THEN
    _options := _options || format('default_column_copy_mode = %L', column_copy_mode);
  END IF;

  IF numeric_noise_fraction IS NOT NULL
  THEN
    _options := _options || format('default_numeric_noise_fraction = %s', numeric_noise_fraction);
  END IF;

  IF date_noise_days IS NOT NULL
  THEN
    _options := _options || format('default_date_noise_days = %s', date_noise_days);
  END IF;

  IF timestamp_noise_seconds IS NOT NULL
  THEN
    _options := _options || format('default_timestamp_noise_seconds = %s', timestamp_noise_seconds);
  END IF;

  IF _options IS NULL
  THEN
    RAISE NOTICE 'no options specified, nothing to do';
    RETURN;
  END IF;

  EXECUTE format(_sql, array_to_string(_options, ', '), _context_name, _schema_name, _table_name);
END;
$$
VOLATILE
LANGUAGE plpgsql;
