CREATE FUNCTION themask.modify_column_rule(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT,
                                           _operation    themask.MASK_OPERATION DEFAULT NULL, _args JSONB DEFAULT NULL
)
  RETURNS VOID AS
$$
DECLARE
  _mask_operation themask.MASK_OPERATION := _operation;
  _arguments      JSONB := _args;
BEGIN
  ASSERT themask._check_table_policy_exists(_context_name, _schema_name, _table_name), 'policy for the table not found';
  ASSERT themask._obj_exists(_schema_name, _table_name, _column_name), 'column does not exist';

  IF NOT themask._check_column_rule_exists(_context_name, _schema_name, _table_name, _column_name)
  THEN
    RAISE NOTICE 'explicit rule for the column does not exist, creating one';
    PERFORM themask.add_column_rule(_context_name, _schema_name, _table_name, _column_name);
  END IF;

  IF _mask_operation IS NULL
  THEN
    SELECT operation
    INTO _mask_operation
    FROM themask.column_rule
    WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name AND
          column_name = _column_name;
  END IF;

  PERFORM themask._check_operation_supported(_schema_name, _table_name, _column_name, _mask_operation);

  IF _arguments IS NULL
  THEN
    SELECT args
    INTO _arguments
    FROM themask.column_rule
    WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name AND
          column_name = _column_name;
  END IF;

  UPDATE themask.column_rule
  SET operation = _mask_operation, args = _arguments
  WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name AND
        column_name = _column_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
