CREATE FUNCTION themask.remove_column_rule(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS VOID AS
$$
BEGIN
  PERFORM themask._get_context(_context_name); -- check if exists

  RAISE NOTICE 'removing %.%.% column rule for masking context %', quote_ident(_schema_name), quote_ident(_table_name),
  quote_ident(_column_name), _context_name;

  DELETE FROM themask.column_rule
  WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name AND
        column_name = _column_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
