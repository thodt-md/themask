CREATE FUNCTION themask.add_context(context_name              TEXT,
                                    schema_prefix             TEXT DEFAULT 'fake_',
                                    temporary_luts            BOOLEAN DEFAULT TRUE,
                                    unlogged_luts             BOOLEAN DEFAULT FALSE,
                                    suffix_for_luts           TEXT DEFAULT '_lut',
                                    unlogged_tables           BOOLEAN DEFAULT FALSE,
                                    truncate_masked_tables    BOOLEAN DEFAULT FALSE,
                                    strict_reference_checking BOOLEAN DEFAULT TRUE


)
  RETURNS VOID AS
$$
BEGIN
  ASSERT coalesce(context_name, '') <> '', 'context name cannot be empty';

  RAISE NOTICE 'adding masking context %', context_name;

  INSERT INTO themask.mask_context (name, masked_schema_prefix, make_temporary_luts, make_unlogged_luts, lut_suffix,
                                    make_unlogged_tables, fail_on_pkey_fkey_checks, truncate_existing_masked_tables)
  VALUES (context_name, schema_prefix, temporary_luts, unlogged_luts, suffix_for_luts, unlogged_tables,
          strict_reference_checking, truncate_masked_tables);

  EXCEPTION WHEN UNIQUE_VIOLATION
  THEN
    RAISE EXCEPTION 'mask context % already exists', context_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
