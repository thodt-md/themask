CREATE FUNCTION themask.modify_context(context_name              TEXT,
                                       schema_prefix             TEXT DEFAULT NULL,
                                       temporary_luts            BOOLEAN DEFAULT NULL,
                                       unlogged_luts             BOOLEAN DEFAULT NULL,
                                       suffix_for_luts           TEXT DEFAULT NULL,
                                       unlogged_tables           BOOLEAN DEFAULT NULL,
                                       truncate_masked_tables    BOOLEAN DEFAULT NULL,
                                       strict_reference_checking BOOLEAN DEFAULT NULL
)
  RETURNS VOID AS
$$
DECLARE
  _sql CONSTANT TEXT := 'UPDATE themask.mask_context SET %s WHERE name = %L';
  _options      TEXT [];
BEGIN
  PERFORM themask._get_context(context_name); -- check if exists

  RAISE NOTICE 'modifying settings for masking context %', context_name;

  IF coalesce(schema_prefix, '') <> '' -- don't allow overwriting existing tables
  THEN
    _options := _options || format('masked_schema_prefix = %L', schema_prefix);
  END IF;

  IF temporary_luts IS NOT NULL
  THEN
    _options := _options || format('make_temporary_luts = %L', temporary_luts);
  END IF;

  IF unlogged_luts IS NOT NULL
  THEN
    _options := _options || format('make_unlogged_luts = %L', unlogged_luts);
  END IF;

  IF suffix_for_luts IS NOT NULL
  THEN
    _options := _options || format('lut_suffix = %L', suffix_for_luts);
  END IF;

  IF unlogged_tables IS NOT NULL
  THEN
    _options := _options || format('make_unlogged_tables = %L', unlogged_tables);
  END IF;

  IF strict_reference_checking IS NOT NULL
  THEN
    _options := _options || format('fail_on_pkey_fkey_checks = %L', strict_reference_checking);
  END IF;

  IF truncate_masked_tables IS NOT NULL
  THEN
    _options := _options || format('truncate_existing_masked_tables = %L', truncate_masked_tables);
  END IF;

  IF _options IS NULL
  THEN
    RAISE NOTICE 'no options specified, nothing to do';
    RETURN;
  END IF;

  EXECUTE format(_sql, array_to_string(_options, ', '), context_name);
END;
$$
VOLATILE
LANGUAGE plpgsql;
