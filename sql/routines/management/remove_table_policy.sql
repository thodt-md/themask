CREATE FUNCTION themask.remove_table_policy(_context_name TEXT, _schema_name TEXT, _table_name TEXT)
  RETURNS VOID AS
$$
BEGIN
  ASSERT themask._check_table_policy_exists(_context_name, _schema_name, _table_name), 'policy for the table not found';

  RAISE NOTICE 'removing %.% table policy for masking context % including column rules', quote_ident(_schema_name),
  quote_ident(_table_name), _context_name;

  -- TODO reset all rule arguments that reference this table policy
  DELETE FROM themask.column_rule
  WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name;
  DELETE FROM themask.table_policy
  WHERE context_name = _context_name AND schema_name = _schema_name AND table_name = _table_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
