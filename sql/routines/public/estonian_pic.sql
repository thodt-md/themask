CREATE FUNCTION themask.estonian_pic(gender VARCHAR(1), birth_date DATE, order_number INT)
  RETURNS TEXT AS
$$
WITH parts AS (
  SELECT
    CASE WHEN lower(gender) = 'f'
      THEN 0
    ELSE -1 END                          AS adj,
    CASE
    WHEN birth_date BETWEEN '1800-01-01' :: DATE AND '1800-12-31' :: DATE
      THEN 2
    WHEN birth_date BETWEEN '1900-01-01' :: DATE AND '1999-12-31' :: DATE
      THEN 4
    WHEN birth_date BETWEEN '2000-01-01' :: DATE AND '2099-12-31' :: DATE
      THEN 6
    WHEN birth_date BETWEEN '2100-01-01' :: DATE AND '2199-12-31' :: DATE
      THEN 8
    END                                  AS prefix,
    to_char(birth_date, 'YYMMDD') :: INT AS numeric_date
), glued AS (
  SELECT themask._estonian_pic_glue_parts(adj + prefix, numeric_date, mod(order_number, 1000)) AS pic
  FROM parts
)
SELECT (pic * 10 + themask._estonian_pic_checksum(pic)) :: TEXT
FROM glued;
$$
IMMUTABLE
STRICT
LANGUAGE SQL;
