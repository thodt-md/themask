CREATE FUNCTION themask.random_gender()
  RETURNS VARCHAR(1) AS
$$
SELECT CASE WHEN themask.random_boolean() THEN 'f' ELSE 'm' END;
$$
VOLATILE
LANGUAGE SQL;
