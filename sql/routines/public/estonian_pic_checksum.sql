CREATE FUNCTION themask._estonian_pic_checksum(glued_pic BIGINT)
  RETURNS INT AS
$$
SELECT CASE WHEN sum1 <> 10
  THEN sum1
       ELSE sum2 END AS result
FROM (
       SELECT
         mod(sum(num * w1), 11) :: INT AS sum1,
         mod(sum(num * w2), 11) :: INT AS sum2
       FROM (
              SELECT
                mod(glued_pic / (10 ^ series.rev) :: BIGINT, 10 :: BIGINT) AS num,
                mod(series.fwd, 9) + 1                                     AS w1,
                mod(series.fwd + 2, 9) + 1                                 AS w2
              FROM (SELECT
                      generate_series(0, 9)     AS fwd,
                      generate_series(9, 0, -1) AS rev) series
            ) checksums
     ) checksum;
$$
IMMUTABLE
STRICT
LANGUAGE SQL;


CREATE FUNCTION themask.estonian_pic_checksum(prefix INT, birth_date INT, order_number INT)
  RETURNS INT AS
$$
SELECT themask._estonian_pic_checksum(themask._estonian_pic_glue_parts(prefix, birth_date, order_number));
$$
IMMUTABLE
STRICT
LANGUAGE SQL;
