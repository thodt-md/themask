CREATE FUNCTION themask.random_signum()
  RETURNS INTEGER AS
$$
SELECT CASE WHEN themask.random_boolean() THEN 1 ELSE -1 END;
$$
VOLATILE
LANGUAGE SQL;
