CREATE FUNCTION themask.random_noise(timestamp_value TIMESTAMP WITH TIME ZONE, seconds DOUBLE PRECISION)
  RETURNS TIMESTAMP WITH TIME ZONE AS
$$
SELECT timestamp_value + make_interval(secs := themask.random_signum() * random() * seconds);
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_noise(date DATE, days INT)
  RETURNS DATE AS
$$
SELECT date + (themask.random_signum() * random() * days) :: INT;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_noise(number NUMERIC, fraction DOUBLE PRECISION)
  RETURNS NUMERIC AS
$$
SELECT number + (number * fraction * random() * themask.random_signum()) :: NUMERIC;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_noise(number SMALLINT, fraction DOUBLE PRECISION)
  RETURNS SMALLINT AS
$$
SELECT least(greatest(rnd.num, -32768), 32767) :: SMALLINT
FROM (SELECT number :: INT + (number * fraction * random() * themask.random_signum()) :: INT AS num) rnd;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_noise(number INT, fraction DOUBLE PRECISION)
  RETURNS INT AS
$$
SELECT least(greatest(rnd.num, -2147483648), 2147483647) :: INT
FROM (SELECT number :: BIGINT + (number * fraction * random() * themask.random_signum()) :: BIGINT AS num) rnd;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_noise(number BIGINT, fraction DOUBLE PRECISION)
  RETURNS BIGINT AS
$$
SELECT least(greatest(themask.random_noise(number :: NUMERIC, fraction), -9223372036854775808),
             9223372036854775807) :: BIGINT;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


