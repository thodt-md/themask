CREATE FUNCTION themask.random_number(range_start NUMERIC, range_end NUMERIC)
  RETURNS NUMERIC AS
$$
SELECT range_start + (range_end - range_start) * random()::NUMERIC;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_number(range_start BIGINT, range_end BIGINT)
  RETURNS BIGINT AS
$$
SELECT themask.random_number(range_start :: NUMERIC, range_end :: NUMERIC) :: BIGINT;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_number(range_start SMALLINT, range_end SMALLINT)
  RETURNS SMALLINT AS
$$
SELECT range_start + ((range_end :: INT - range_start :: INT) * random()) :: SMALLINT;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_number(range_start INT, range_end INT)
  RETURNS INT AS
$$
SELECT range_start + ((range_end :: BIGINT - range_start :: BIGINT) * random()) :: INT;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_number(range_start REAL, range_end REAL)
  RETURNS REAL AS
$$
SELECT range_start + ((range_end :: DOUBLE PRECISION - range_start :: DOUBLE PRECISION) * random()) :: REAL;
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;


CREATE FUNCTION themask.random_number(range_start DOUBLE PRECISION, range_end DOUBLE PRECISION)
  RETURNS DOUBLE PRECISION AS
$$
SELECT range_start + (range_end - range_start) * random();
$$
VOLATILE
RETURNS NULL ON NULL INPUT
LANGUAGE SQL;
