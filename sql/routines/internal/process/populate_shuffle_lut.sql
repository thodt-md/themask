CREATE FUNCTION themask._populate_shuffle_lut(_lut_name      TEXT, _source_schema TEXT, _source_table TEXT,
                                              _source_column TEXT, _source_pkey_column TEXT)
  RETURNS VOID AS
$$
DECLARE
  _temp_table_sql CONSTANT TEXT := 'CREATE TEMP TABLE %1$I (id BIGSERIAL PRIMARY KEY, val %2$s) ON COMMIT DROP';
  _temp_trunc_sql CONSTANT TEXT := 'TRUNCATE %1$I';
  _id_insert_sql CONSTANT  TEXT := 'INSERT INTO %1$I (val) SELECT %2$I FROM %3$I.%4$I';
  _val_insert_sql CONSTANT TEXT := 'INSERT INTO %1$I (val) SELECT %2$I FROM %3$I.%4$I ORDER BY random()';
  _sql CONSTANT            TEXT := 'INSERT INTO %1$I (original, masked) SELECT t1.id AS original, t2.val AS masked' ||
                                   ' FROM %2$I t1, %3$I t2 WHERE t1.id = t2.id';
  _id_table_name           TEXT;
  _value_table_name        TEXT;
  _source_type             TEXT;
  _pkey_type               TEXT;
  _actual_row_count        BIGINT;
BEGIN
  -- this is probably VERY inefficient, needs refactoring
  RAISE NOTICE 'populating shuffled LUT % from %.%.% (using %)', quote_ident(_lut_name), quote_ident(_source_schema),
  quote_ident(_source_table), quote_ident(_source_column), quote_ident(_source_pkey_column);

  _source_type := themask._get_column_type_def(_source_schema, _source_table, _source_column);
  _pkey_type := themask._get_column_type_def(_source_schema, _source_table, _source_pkey_column);

  _id_table_name := _source_table || 'ids' || (random() * 1000) :: INT;
  _value_table_name := _source_table || 'val' || (random() * 1000) :: INT;

  EXECUTE format(_temp_table_sql, _id_table_name, _pkey_type);
  EXECUTE format(_temp_table_sql, _value_table_name, _source_type);
  EXECUTE format(_id_insert_sql, _id_table_name, _source_pkey_column, _source_schema, _source_table);
  EXECUTE format(_val_insert_sql, _value_table_name, _source_column, _source_schema, _source_table);
  EXECUTE format(_sql, _lut_name, _id_table_name, _value_table_name);

  GET DIAGNOSTICS _actual_row_count = ROW_COUNT;

  EXECUTE format(_temp_trunc_sql, _id_table_name);
  EXECUTE format(_temp_trunc_sql, _value_table_name);

  RAISE NOTICE 'LUT % populated with % shuffled row(s)', quote_ident(_lut_name), _actual_row_count;
END;
$$
VOLATILE
STRICT
LANGUAGE plpgsql;


CREATE FUNCTION themask._populate_shuffle_lut(_context_name TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _lut_name    TEXT;
  _pkey_column TEXT;
BEGIN
  _lut_name := themask._get_lut_name(_context_name, _schema_name, _table_name, _column_name);
  ASSERT _lut_name IS NOT NULL, 'LUT name cannot be NULL';

  SELECT pkey_column
  INTO STRICT _pkey_column
  FROM themask._pkeys
  WHERE schema_name = _schema_name AND table_name = _table_name;
  ASSERT _pkey_column IS NOT NULL, 'PK column name cannot be NULL';

  PERFORM themask._populate_shuffle_lut(_lut_name, _schema_name, _table_name, _column_name, _pkey_column);
END;
$$
VOLATILE
STRICT
LANGUAGE plpgsql;
