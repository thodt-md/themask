CREATE FUNCTION themask._restore_constraints(_context_name TEXT)
  RETURNS VOID AS
$$
BEGIN
  -- TODO implement
  RAISE NOTICE 'restoring table constraints for mask context %', _context_name;
END;
$$
VOLATILE
LANGUAGE plpgsql;
