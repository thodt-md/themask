CREATE FUNCTION themask.__populate_lut(_lut_name      TEXT, _source_schema TEXT, _source_table TEXT,
                                       _source_column TEXT, _population_function TEXT)
  RETURNS VOID AS
$$
DECLARE
  _sql CONSTANT       TEXT := 'INSERT INTO %1$I (original, masked)' ||
                              ' SELECT a.%2$I AS original, %3$s AS masked FROM %4$I.%5$I a' ||
                              ' WHERE a.%2$I IS NOT NULL ON CONFLICT DO NOTHING';
  _sql_count CONSTANT TEXT := 'SELECT count(*) FROM %1$I.%2$I';
  _source_row_count   BIGINT;
  _actual_row_count   BIGINT;
BEGIN
  RAISE NOTICE 'populating LUT % from %.%.%', quote_ident(_lut_name), quote_ident(_source_schema),
  quote_ident(_source_table), quote_ident(_source_column);

  EXECUTE format(_sql_count, _source_schema, _source_table)
  INTO _source_row_count;

  EXECUTE format(_sql, _lut_name, _source_column, _population_function, _source_schema, _source_table);

  GET DIAGNOSTICS _actual_row_count = ROW_COUNT;
  RAISE NOTICE 'LUT % populated with % row(s) obtained from % source table row(s)', quote_ident(
    _lut_name), _actual_row_count, _source_row_count;
END;
$$
VOLATILE
STRICT
LANGUAGE plpgsql;


CREATE FUNCTION themask._populate_lut(_context_name        TEXT, _schema_name TEXT, _table_name TEXT, _column_name TEXT,
                                      _population_function TEXT)
  RETURNS VOID AS
$$
DECLARE
  _lut_name TEXT;
BEGIN
  _lut_name := themask._get_lut_name(_context_name, _schema_name, _table_name, _column_name);

  ASSERT _lut_name IS NOT NULL, 'LUT name cannot be NULL';

  PERFORM themask.__populate_lut(_lut_name, _schema_name, _table_name, _column_name, _population_function);
END;
$$
VOLATILE
STRICT
LANGUAGE plpgsql;
