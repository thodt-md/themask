CREATE FUNCTION themask._check_pkey_methods(_context_name TEXT, _do_fail BOOLEAN)
  RETURNS VOID AS
$$
DECLARE
  _mask_pkey    themask._PKEYS;
  _table_policy themask.TABLE_POLICY;
BEGIN
  FOR _mask_pkey IN SELECT mp.*
                    FROM themask._pkeys mp INNER JOIN themask._default_operations mdo USING (schema_name, table_name)
                    WHERE mdo.operation = 'nullify' AND mp.pkey_column = mdo.column_name AND
                          mdo.context_name = _context_name
  LOOP
    IF NOT _do_fail
    THEN
      RAISE WARNING 'nullify on primary key column in %.%.%', quote_ident(_mask_pkey.schema_name),
      quote_ident(_mask_pkey.table_name), quote_ident(_mask_pkey.pkey_column);
    ELSE
      RAISE EXCEPTION 'nullify on primary key column in %.%.%', quote_ident(_mask_pkey.schema_name),
      quote_ident(_mask_pkey.table_name), quote_ident(_mask_pkey.pkey_column);
    END IF;
  END LOOP;

  FOR _table_policy IN SELECT *
                       FROM themask.table_policy mtp
                       WHERE mtp.context_name = _context_name
                             AND EXISTS(SELECT 1
                                        FROM themask.column_rule mcr
                                        WHERE mcr.operation = 'shuffle' AND mcr.context_name = mtp.context_name
                                              AND mcr.schema_name = mtp.schema_name AND mcr.table_name = mtp.table_name
                                              AND
                                              EXISTS(
                                                SELECT
                                                  count(*),
                                                  mpk.schema_name,
                                                  mpk.table_name
                                                FROM themask._pkeys mpk
                                                WHERE
                                                  mpk.schema_name = mcr.schema_name AND mpk.table_name = mcr.table_name
                                                GROUP BY mpk.schema_name, mpk.table_name
                                                HAVING count(*) > 1
                                              )
                             )
  LOOP
    -- currently not supported, fail always
    RAISE EXCEPTION 'composite primary keys not supported with shuffle masking method';
  END LOOP;
END;
$$
STABLE
LANGUAGE plpgsql;
