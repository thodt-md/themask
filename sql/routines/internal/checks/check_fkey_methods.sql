CREATE FUNCTION themask._check_fkey_methods(_context_name TEXT, _do_fail BOOLEAN)
  RETURNS VOID AS
$$
DECLARE
  _mask_fkey themask._FKEYS;
BEGIN
  FOR _mask_fkey IN SELECT mf.*
                    FROM themask._fkeys mf, themask._default_operations pmdo, themask._default_operations fmdo
                    WHERE mf.pkey_schema = pmdo.schema_name AND mf.pkey_table = pmdo.table_name AND
                          mf.pkey_column = pmdo.column_name AND mf.fkey_schema = fmdo.schema_name AND
                          mf.fkey_table = fmdo.table_name AND mf.fkey_column = fmdo.column_name AND
                          pmdo.context_name = fmdo.context_name AND pmdo.operation <> fmdo.operation AND
                          pmdo.context_name = _context_name

  LOOP
    IF NOT _do_fail
    THEN
      RAISE WARNING 'masking methods on primary key column %.%.% and foreign key column %.%.% do not match',
      quote_ident(_mask_fkey.pkey_schema), quote_ident(_mask_fkey.pkey_table), quote_ident(_mask_fkey.pkey_column),
      quote_ident(_mask_fkey.fkey_schema), quote_ident(_mask_fkey.fkey_table), quote_ident(_mask_fkey.fkey_column);
    ELSE
      RAISE EXCEPTION 'masking methods on primary key column %.%.% and foreign key column %.%.% do not match',
      quote_ident(_mask_fkey.pkey_schema), quote_ident(_mask_fkey.pkey_table), quote_ident(_mask_fkey.pkey_column),
      quote_ident(_mask_fkey.fkey_schema), quote_ident(_mask_fkey.fkey_table), quote_ident(_mask_fkey.fkey_column);
    END IF;
  END LOOP;
END;
$$
VOLATILE
LANGUAGE plpgsql;
