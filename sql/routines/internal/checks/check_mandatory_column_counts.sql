CREATE FUNCTION themask._check_mandatory_column_counts(_context_name TEXT)
  RETURNS VOID AS
$$
DECLARE
  _mandatory_undeclared TEXT;
BEGIN
  SELECT string_agg(format('%I.%I', schema_name, table_name), ', ')
  INTO _mandatory_undeclared
  FROM themask._get_mandatory_column_counts(_context_name)
  WHERE mandatory_count <> declared_count;

  IF _mandatory_undeclared IS NOT NULL
  THEN
    RAISE EXCEPTION 'declared and available column count does not match'
    USING HINT = 'check tables: ' || _mandatory_undeclared;
  END IF;
END;
$$
STABLE
LANGUAGE plpgsql;
