CREATE FUNCTION themask._get_nullify_populate_function()
  RETURNS TEXT AS
$$
BEGIN
  RETURN 'null';
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
