CREATE FUNCTION themask._get_random_populate_function(_simple_type TEXT, _args JSONB)
  RETURNS TEXT AS
$$
DECLARE
  _textual_type TEXT;
  _textual_args TEXT;
  _function     TEXT;
BEGIN
  CASE
    WHEN _simple_type IN ('int8', 'int4', 'int2', 'float4', 'float8', 'numeric')
         AND jsonb_typeof(_args -> 'range_start') = 'number' AND jsonb_typeof(_args -> 'range_end') = 'number'
    THEN
      _function := format('themask.random_number(%s, %s)', _args ->> 'range_start', _args ->> 'range_end');
    WHEN _simple_type IN ('timestamp', 'timestamptz')
    THEN
      IF jsonb_typeof(_args -> 'range_start') = 'string' AND jsonb_typeof(_args -> 'range_end') = 'string'
      THEN
        _function := format('themask.random_timestamp(%L, %L)', _args ->> 'range_start', _args ->> 'range_end');
      ELSE
        _function := 'themask.random_timestamp()';
      END IF;
    WHEN _simple_type = 'date'
    THEN
      IF jsonb_typeof(_args -> 'range_start') = 'string' AND jsonb_typeof(_args -> 'range_end') = 'string'
      THEN
        _function := format('themask.random_date(%L, %L)', _args ->> 'range_start', _args ->> 'range_end');
      ELSE
        _function := 'themask.random_date()';
      END IF;
    WHEN _simple_type IN ('varchar', 'text') AND _args ? 'type'
    THEN
      _textual_type := _args ->> 'type';
      IF jsonb_typeof(_args -> 'args') = 'string'
      THEN
        _textual_args := _args ->> 'args';
      ELSE
        _textual_args := '';
      END IF;

      IF _textual_type = 'name'
      THEN
        _function := format('themask.random_full_name(%s)', _textual_args);
      ELSEIF _textual_type = 'estonian_pic'
        THEN
          _function := format('themask.random_estonian_pic(%s)', _textual_args);
      END IF;
  END CASE;
  RETURN _function;
END;
$$
IMMUTABLE
STRICT
LANGUAGE plpgsql;
