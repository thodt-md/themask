CREATE FUNCTION themask._get_population_fragment(_operation themask._DEFAULT_OPERATIONS, _table_alias TEXT)
  RETURNS TEXT AS
$$
DECLARE
  _mask_operation     themask.MASK_OPERATION := _operation.operation;
  _context_name       TEXT := _operation.context_name;
  _column_name        TEXT := _operation.column_name;
  _args               JSONB := _operation.args;
  _column_type        TEXT := _operation.column_type;
  _column_simple_type TEXT := _operation.column_simple_type;
  _schema_name        TEXT := _operation.schema_name;
  _table_name         TEXT := _operation.table_name;
  _fragment           TEXT;
BEGIN
  RAISE DEBUG 'making masked table population fragment for %.%.%', quote_ident(_schema_name), quote_ident(_table_name),
  quote_ident(_column_name);

  CASE
    WHEN _mask_operation = 'nullify'
    THEN _fragment := themask._get_nullify_populate_function();
    WHEN _mask_operation = 'copy'
    THEN _fragment := themask._get_copy_populate_function(_column_name, _table_alias);
    WHEN _mask_operation = 'literal'
    THEN _fragment := themask._get_literal_populate_function(_column_type, _args);
    WHEN _mask_operation = 'noise'
    THEN _fragment := themask._get_noise_populate_function(_column_name, _column_simple_type, _args, _table_alias);
    WHEN _mask_operation = 'random'
    THEN _fragment := themask._get_random_populate_function(_column_simple_type, _args);
    WHEN _mask_operation = 'truncate'
    THEN _fragment := themask._get_truncate_populate_function(_column_name, _column_simple_type, _args, _table_alias);
    WHEN _mask_operation = 'mask'
    THEN _fragment := themask._get_mask_populate_function(_column_name, _column_simple_type, _args, _table_alias);
    WHEN _mask_operation = 'lut'
    THEN _fragment := themask._get_lut_populate_function(_context_name, _column_name, _args, _table_alias);
    WHEN _mask_operation = 'random_lut'
    THEN _fragment := themask._get_random_lut_populate_function(_context_name, _schema_name, _table_name, _column_name,
                                                                _table_alias);
    WHEN _mask_operation = 'shuffle'
    THEN _fragment := themask._get_shuffle_lut_populate_function(_context_name, _schema_name, _table_name, _column_name,
                                                                 _args, _table_alias);
  ELSE
    RAISE EXCEPTION 'unsupported masking operation: %', _mask_operation;
  END CASE;

  RETURN _fragment;
END;
$$
STRICT
STABLE
LANGUAGE plpgsql;
