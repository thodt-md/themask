CREATE FUNCTION themask._get_lut_populate_function(_lut_name TEXT, _column_name TEXT, _table_alias TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
BEGIN
  RETURN format('(SELECT lut.masked FROM %I lut WHERE lut.original = %s.%I)', _lut_name, _table_alias, _column_name);
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;


CREATE FUNCTION themask._get_lut_populate_function(_context_name TEXT, _column_name TEXT, _args JSONB,
                                                   _table_alias  TEXT DEFAULT 'a')
  RETURNS TEXT AS
$$
DECLARE
  _lut_name TEXT;
BEGIN
  IF jsonb_typeof(_args -> 'lut_name') = 'string'
  THEN
    _lut_name := _args ->> 'lut_name';
  ELSEIF jsonb_typeof(_args -> 'schema') = 'string' AND jsonb_typeof(_args -> 'table') = 'string'
         AND jsonb_typeof(_args -> 'column') = 'string'
    THEN
      _lut_name = themask._get_lut_name(_context_name, _args ->> 'schema', _args ->> 'table', _args ->> 'column');
  END IF;

  RETURN themask._get_lut_populate_function(_lut_name, _column_name, _table_alias);
END;
$$
STRICT
STABLE
LANGUAGE plpgsql;
