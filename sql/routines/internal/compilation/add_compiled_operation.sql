CREATE FUNCTION themask._add_compiled_operation(_context_name TEXT, _sql TEXT)
  RETURNS INT AS
$$
INSERT INTO themask.compiled_operation (context_name, operation_order, operation_sql)
VALUES (_context_name, coalesce((SELECT max(m.operation_order)
                                 FROM themask.compiled_operation m
                                 WHERE context_name = _context_name), 0) + 1, _sql)
RETURNING operation_order;
$$
VOLATILE
LANGUAGE SQL;
