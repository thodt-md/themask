CREATE FUNCTION themask._get_literal_populate_function(_column_type TEXT, _args JSONB)
  RETURNS TEXT AS
$$
DECLARE
  _literal TEXT;
  _cast    TEXT;
BEGIN
  _literal := _args ->> 'value';
  IF _literal IS NULL
  THEN
    RETURN NULL;
  END IF;

  IF coalesce(trim(_literal), '') = '' AND (lower(_column_type) <> 'text' OR lower(_column_type) LIKE '%char%')
  THEN
    RETURN themask._get_nullify_populate_function();
  END IF;

  _cast := '::' || _column_type;

  RETURN format('(%s)%s', _literal, _cast);
END;
$$
STRICT
IMMUTABLE
LANGUAGE plpgsql;
