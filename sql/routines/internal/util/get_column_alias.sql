CREATE FUNCTION themask._get_column_alias(_schema_name TEXT, _table_name TEXT, _alias_prefix TEXT)
  RETURNS TEXT AS
$$
DECLARE
  _column_name TEXT;
  _existing_count BIGINT;
  _counter BIGINT;
BEGIN
  LOOP
    _column_name = _alias_prefix || coalesce(_counter::text, '');

    SELECT count(*)
    INTO _existing_count
    FROM themask._col_defs d
    WHERE d.schema_name = _schema_name AND d.table_name = _table_name AND d.column_name = _column_name;

    IF _existing_count = 0 THEN
      EXIT;
    END IF;

    _counter := coalesce(_counter, 0) + 1;
  END LOOP;

  RETURN _column_name;
END;
$$
STABLE
STRICT
LANGUAGE plpgsql;


CREATE FUNCTION themask._get_column_alias(_schema_name TEXT, _table_name TEXT, _alias_prefix TEXT, _column_name TEXT)
  RETURNS TEXT AS
$$
BEGIN
  RETURN themask._get_column_alias(_schema_name, _table_name, _alias_prefix || _column_name);
END;
$$
STABLE
STRICT
LANGUAGE plpgsql;
