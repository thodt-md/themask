CREATE FUNCTION themask._get_masked_schema_name(_context themask.MASK_CONTEXT, _schema_name TEXT)
  RETURNS TEXT AS
$$
SELECT _context.masked_schema_prefix || _schema_name;
$$
LANGUAGE SQL
IMMUTABLE
RETURNS NULL ON NULL INPUT;


CREATE FUNCTION themask._get_masked_schema_name(_context_name TEXT, _schema_name TEXT)
  RETURNS TEXT AS
$$
SELECT themask._get_masked_schema_name(context, _schema_name)
FROM themask.mask_context context
WHERE context.name = _context_name;
$$
LANGUAGE SQL
STABLE
RETURNS NULL ON NULL INPUT;
