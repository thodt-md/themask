CREATE FUNCTION themask._get_context(_context_name TEXT)
  RETURNS themask.MASK_CONTEXT AS
$$
DECLARE
  _context themask.MASK_CONTEXT%ROWTYPE;
BEGIN
  SELECT *
  INTO _context
  FROM themask.mask_context
  WHERE name = _context_name;

  IF NOT FOUND
  THEN
    RAISE EXCEPTION 'masking context % not found', _context_name;
  END IF;

  RETURN _context;
END;
$$
STABLE
LANGUAGE plpgsql;
