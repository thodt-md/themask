CREATE FUNCTION themask._obj_exists(_schema_name TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) > 0
FROM themask._col_defs
WHERE schema_name = _schema_name;
$$
LANGUAGE SQL
STABLE;


CREATE FUNCTION themask._obj_exists(_schema_name TEXT, _table_name TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) > 0
FROM themask._col_defs
WHERE schema_name = _schema_name AND table_name = _table_name;
$$
LANGUAGE SQL
STABLE;


CREATE FUNCTION themask._obj_exists(_schema_name TEXT, _table_name TEXT, _column_name TEXT)
  RETURNS BOOLEAN AS
$$
SELECT count(*) > 0
FROM themask._col_defs
WHERE schema_name = _schema_name AND table_name = _table_name AND column_name = _column_name;
$$
LANGUAGE SQL
STABLE;
