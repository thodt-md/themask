EXTENSION = themask
EXTVERSION = $(shell grep default_version $(EXTENSION).control | \
               sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")
PG_CONFIG = pg_config
PGVER = $(shell $(PG_CONFIG) --version | sed 's/[^0-9\.]//g' | awk -F . '{ print $$1$$2 }')

ifeq ($(PGVER),96)

# paths to the database entities
typesdir = sql/types/
tablesdir = sql/tables/
viewsdir = sql/views/

introutines = sql/routines/internal/
checkroutines = ${introutines}checks/
comproutines = ${introutines}compilation/
procroutines = ${introutines}process/
utilroutines = ${introutines}util/

mgmtroutines = sql/routines/management/
pubroutines = sql/routines/public/

# path for miscellaneous fragments needed in the final extension script
fragsdir = sql/fragments/

# entities for each type in dependency order
# while it is probably more easy to use wildcards for paths with no entities, that depend on each other,
# this is not always the case, we define everything explicitly for a more repeatable build
typesdep =	${typesdir}mask_operation.sql \
		${typesdir}mask_default_column_mode.sql

tablesdep =	${tablesdir}mask_context.sql \
		${tablesdir}table_policy.sql \
		${tablesdir}column_rule.sql \
		${tablesdir}compiled_operation.sql \
		${tablesdir}popular_name.sql

viewsdep =	${viewsdir}col_defs.sql \
		${viewsdir}con_unfolded.sql \
		${viewsdir}tables.sql \
		${viewsdir}pkeys.sql \
		${viewsdir}fkeys.sql \
		${viewsdir}default_operations.sql \
		${viewsdir}effective_operations.sql

routinesdep = ${pubroutines}estonian_pic_glue_parts.sql \
		${pubroutines}estonian_pic_checksum.sql \
		${pubroutines}estonian_pic.sql \
		${pubroutines}random_boolean.sql \
		${pubroutines}random_timestamp.sql \
		${pubroutines}random_date.sql \
		${pubroutines}random_gender.sql \
		${pubroutines}random_number.sql \
		${pubroutines}random_estonian_pic.sql \
		${pubroutines}random_name.sql \
		${pubroutines}random_full_name.sql \
		${pubroutines}random_signum.sql \
		${pubroutines}random_noise.sql \
		${pubroutines}text_mask.sql \
		${utilroutines}get_column_alias.sql \
		${utilroutines}get_column_type_def.sql \
		${utilroutines}get_context.sql \
		${utilroutines}get_lut_name.sql \
		${utilroutines}get_mandatory_column_counts.sql \
		${utilroutines}get_masked_schema_name.sql \
		${utilroutines}obj_exists.sql \
		${checkroutines}check_column_rule_exists.sql \
		${checkroutines}check_fkey_methods.sql \
		${checkroutines}check_mandatory_column_counts.sql \
		${checkroutines}check_operation_supported.sql \
		${checkroutines}check_pkey_methods.sql \
		${checkroutines}check_table_nop.sql \
		${checkroutines}check_table_policy_exists.sql \
		${checkroutines}check_shuffle_has_pkeys.sql \
		${procroutines}create_lut.sql \
		${procroutines}create_masked_schemas.sql \
		${procroutines}create_masked_tables.sql \
		${procroutines}create_shuffle_lut.sql \
		${procroutines}populate_lut.sql \
		${procroutines}populate_shuffle_lut.sql \
		${procroutines}restore_constraints.sql \
		${comproutines}add_compiled_operation.sql \
		${comproutines}add_independent_random_lut_columns.sql \
		${comproutines}add_independent_shuffle_columns.sql \
		${comproutines}get_copy_populate_function.sql \
		${comproutines}get_nullify_populate_function.sql \
		${comproutines}get_literal_populate_function.sql \
		${comproutines}get_lut_populate_function.sql \
		${comproutines}get_mask_populate_function.sql \
		${comproutines}get_noise_populate_function.sql \
		${comproutines}get_nullify_populate_function.sql \
		${comproutines}get_random_lut_populate_function.sql \
		${comproutines}get_random_populate_function.sql \
		${comproutines}get_shuffle_lut_populate_function.sql \
		${comproutines}get_truncate_populate_function.sql \
		${comproutines}get_population_fragment.sql \
		${mgmtroutines}add_context.sql \
		${mgmtroutines}add_table_policy.sql \
		${mgmtroutines}add_column_rule.sql \
		${mgmtroutines}modify_column_rule.sql \
		${mgmtroutines}modify_table_policy.sql \
		${mgmtroutines}modify_context.sql \
		${mgmtroutines}remove_column_rule.sql \
		${mgmtroutines}remove_table_policy.sql \
		${mgmtroutines}remove_context.sql \
		${mgmtroutines}compile_rules.sql \
		${mgmtroutines}run.sql

all: sql/$(EXTENSION)--$(EXTVERSION).sql

sql/$(EXTENSION)--$(EXTVERSION).sql: ${fragsdir}header.sql ${typesdep} ${tablesdep} \
		${fragsdir}config_tables.sql ${viewsdep} ${routinesdep}
	cat > $@ $^

DATA = sql/$(EXTENSION)--$(EXTVERSION).sql
EXTRA_CLEAN = sql/$(EXTENSION)--$(EXTVERSION).sql
else
$(error Minimum version of PostgreSQL required is 9.6.0)
endif

PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
