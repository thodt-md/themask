#!/bin/bash
docker cp ../data/names/dist.all.last.csv docker_db-pgxs_1:/tmp
docker cp ../data/names/dist.female.first.csv docker_db-pgxs_1:/tmp
docker cp ../data/names/dist.male.first.csv docker_db-pgxs_1:/tmp
docker cp ../data/names/load_census_names.sql docker_db-pgxs_1:/tmp
docker cp ../data/names/load_census_names.sh docker_db-pgxs_1:/tmp
docker exec docker_db-pgxs_1 psql -vworkdir="/tmp" -U postgres -d postgres -a -f /tmp/load_census_names.sql
