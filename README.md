# PostgreSQL Data Masking Extension

## A Project for Master's Thesis

This extension was developed as part of my Master's Thesis as a proof-of-concept automated data masking solution
for PostgreSQL.

### License

Published under MIT license. Full license text available in LICENSE file.


### Requirements

0.  Debian/Ubuntu. As of now not tested on anything else.
1.  PostgreSQL 9.6+. Not tested with older versions.
2.  postgresql-server-dev-9.6 package (PGXS).
3.  build-essentials package (GNU make and other utilities, that may be needed during build). 


### Installation

1.  Checkout to a clean location
2.  ``make``
3.  ``make install``
4.  ``CREATE EXTENSION themask;``
5.  (optional) Copy US Census popular names from data/names   


### Usage

1.  ``SELECT themask.add_context('mycontext');``
2.  ``SELECT themask.add_column_rule('mycontext', 'myschema', 'mytable', 'id', 'copy');``
3.  ``SELECT themask.add_column_rule('mycontext', 'myschema', 'mytable', 'name', 'random', '{"type": "name"}');``
4.  ``SELECT themask.compile_rules('mycontext');``
5.  ``SELECT themask.run('mycontext');``


### Sources

1.  Names for random name generator taken from 1990 US Census page: https://www.census.gov/topics/population/genealogy/data/1990_census/1990_census_namefiles.html
    
    Data file redistribution is unrestricted, as stated here: https://www.usa.gov/government-works 
